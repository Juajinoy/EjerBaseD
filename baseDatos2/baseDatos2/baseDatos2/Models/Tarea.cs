﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace baseDatos2.Models
{
    // guardamos los campos de la tabla
    //id
    // nombre de la tarea
    //fecha de finalizacion
    // hora de finalizacion
    // completado
    class Tarea
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        [MaxLength(150)] // solo va a admitir 150 caracteres
        public string Name { get; set; }
        public DateTime Datefinish { get; set; }
        public TimeSpan HourFinish { get; set; }
        public Boolean Completed { get; set; }
    }
}
