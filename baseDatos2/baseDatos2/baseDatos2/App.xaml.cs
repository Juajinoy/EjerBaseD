﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace baseDatos2
{
	public partial class App : Application
	{

        //DEFINICION DE ATRIBUTO GENERAL esta variable va a tener la ruta en todos los proyectos
        public static string urlBd;

        public App ()
		{
			InitializeComponent();

			MainPage = new baseDatos2.MainPage();
		}

        //sobre escribir el metodo APP para utilizar la base de datos
        // se conoce que es metodo constructos porque tiene el mismo nombre de la clase en este caso APP
        // el sobreescribir un metodo contructor en JAVA se llama sobrecarga

        //INICIO DE CODIGO DE SOBREESCRITURA
        public App(string url)
        {
            InitializeComponent();

            urlBd = url;

            MainPage = new baseDatos2.MainPage();
        }
        //FIN DE METODO DE SOBREESCRITURA


        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
