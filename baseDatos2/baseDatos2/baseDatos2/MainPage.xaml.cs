﻿using baseDatos2.Models;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace baseDatos2
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        public void CreateTask(object sender, EventArgs e)
        {
            //crear objeto 
            Tarea tarea = new Tarea()
            {
                Name = name.Text,
                Datefinish = dateFinish.Date,
                HourFinish = timeFinish.Time,
                Completed = false
            };
            // lo utilizo para no duplicar variables
            // voy a declarar la conexion en using para que solo se ejecute una sola ves dentro de la aplicacion
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // con esta linea creo una tabla en mi base de datos
                connection.CreateTable<Tarea>();
                //Para guardar el objeto de tipo tarea
                //connection.Insert(tarea);

                // crear un condicional para resultado de conexion exitosa

                var result = connection.Insert(tarea);
                if (result >0)
                {
                    DisplayAlert("Correcto","La tarea se creo correctamente","OK");

                }
                else
                {
                    DisplayAlert("Error", "La tarea no fue creada", "OK");
                }
            }
        }
	}
}
