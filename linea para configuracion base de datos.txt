
        //DEFINICION DE ATRIBUTO GENERAL esta variable va a tener la ruta en todos los proyectos
        public static string urlBd;

        public App ()
		{
			InitializeComponent();

			MainPage = new baseDatos2.MainPage();
		}

        //sobre escribir el metodo APP para utilizar la base de datos
        // se conoce que es metodo constructos porque tiene el mismo nombre de la clase en este caso APP
        // el sobreescribir un metodo contructor en JAVA se llama sobrecarga

        //INICIO DE CODIGO DE SOBREESCRITURA
        public App(string url)
        {
            InitializeComponent();

            urlBd = url;

            MainPage = new baseDatos2.MainPage();
        }


_____________________________________________________________________________
LINEA DE CONFIGURACION PARA ANDROID

-> string name_file_db = "tareas.sqlite"; // obtiene el archivo donde se almacenara

//obtiene la direccion de donde se encuentra la base de datos
-> string path_android = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

//traer la ruta
-> string path_db = Path.Combine(path_android, name_file_db);      


LO COLOCAMOS EN ESTA LINEA

protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            -> string name_file_db = "tareas.sqlite"; // obtiene el archivo donde se almacenara

            //obtiene la direccion de donde se encuentra la base de datos
            -> string path_android = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            //traer la ruta
            -> string path_db = Path.Combine(path_android, name_file_db);

            LoadApplication(new App(path_db));
        }


______________________________________________________________________________________
PARA CONFIGURAR IOS
______________________________________________________________________________________

public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            string name_file_db = "tareas.sqlite"; 
            string path_ios = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path_ios_final = Path.Combine(path_ios, "..", "Library"); //le envio comando para salir de la carpeta actual y pasar a Library
            string path_db = Path.Combine(path_ios_final, name_file_db);

            LoadApplication(new App(path_db));

            return base.FinishedLaunching(app, options);
        }